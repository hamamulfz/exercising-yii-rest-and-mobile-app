<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "artikel".
 *
 * @property int $id
 * @property string $judul
 * @property string $isi_artikel
 * @property string $create_at
 * @property string $update_at
 */
class Artikel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'artikel';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'judul', 'create_at'], 'required'],
            [['id'], 'integer'],
            [['isi_artikel'], 'string'],
            [['create_at', 'update_at'], 'safe'],
            [['judul'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'judul' => 'Judul',
            'isi_artikel' => 'Isi Artikel',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
        ];
    }
}
