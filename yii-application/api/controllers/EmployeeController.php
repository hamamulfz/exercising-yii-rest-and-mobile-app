<?php

namespace api\controllers;
use api\models\Employee;
use api\models\EmployeeForm;
use Yii;

class EmployeeController extends \yii\rest\Controller
{

    public function behaviors()
    {
        $behavior = parent::behaviors();
        $behavior['contentNegotiator']['formats']['text/html'] = \yii\web\Response::FORMAT_JSON;
        $behavior['authenticator'] = [
            'class' => \yii\filters\auth\HttpBearerAuth::className(),
        ];
        return $behavior;
    }

    protected function verbs()
    {
        return [
            'create' => ['POST'],
            'delete' => ['DELETE'],
        ];
    }

    public function actionRead()
    {   $employee = Employee::find()->all();
        if(!$employee){
            return  [ 
                'status' => 'error',
                'data' => 'data not found'
            ];
        }
        else {
            return  [ 
                'status' => 'success',
                'data' => $employee
            ];
        }
    }

    public function actionCreate()
    {
        $model = new EmployeeForm();
        $model->load(Yii::$app->request->post(),'');
        if($model->validate()){

            $model_employee= new Employee();
            $model_employee->name = $model->name;
            $model_employee->email = $model->email;
            $model_employee->birthday = $model->birthday;
            $model_employee->photo = $model->photo;

            if($model_employee->validate() && $model_employee->save() ){
                    return ['status'=>'success',
                        'data' => 'data berhasil disimpan'
                    ];

            } else {
                return ['status'=>'fail',
                        'message' => $model_employee->errors,
                        ];
            }
        }else {
            return [
                'status' => 'error',
                'message' => $model->errors,
                // 'data' => '',
            ];
        }
    }

    public function actionUpdate()
    {
        $model = new EmployeeForm();
        $model->load(Yii::$app->request->post(),'');
        if($model->validate()){
            $model_employee= Employee::findOne($model->id);
            $model_employee->name = $model->name;
            $model_employee->email = $model->email;
            $model_employee->birthday = $model->birthday;
            $model_employee->photo = $model->photo;

            if($model_employee->validate() && $model_employee->save() ){
                    return ['status'=>'success',
                        'data' => 'data berhasil disimpan'
                    ];

            } else {
                return ['status'=>'fail',
                        'message' => $model_employee->errors,
                        ];
            }
        }else {
            return [
                'status' => 'error',
                'message' => $model->errors,
            ];
        }
    }

    public function actionDelete()
    {
        $model = new EmployeeForm();
        $model->load(Yii::$app->request->post(),'');
        if($model->validate()){
            $model_employee= Employee::findOne($model->id);
            $model_employee->delete();
            return ['status'=>'success',
            'data' => 'data berhasil dihapus'
        ];
        }else {
            return [
                'status' => 'error',
                'message' => $model->errors,
            ];
        }
    }

    public function actionSearch()
    {
        $nama = Yii::$app->getRequest()->getBodyParams('name');
        $data = Employee::find()->where(['like', 'name', $nama])->all();
        if(!$data){
            return  [ 
                'status' => 'error',
                'data' => 'data not found'
            ];
        }
        else {
            return  [ 
                'status' => 'success',
                'data' => $data
            ];
        }
    }

}
