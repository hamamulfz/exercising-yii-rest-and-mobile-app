<?php
namespace api\controllers;
use Yii;

use common\models\User;
 
class LogoutController extends \yii\rest\Controller
{
    public function behaviors()
    {
        $behavior = parent::behaviors();
        $behavior['contentNegotiator']['formats']['text/html'] = \yii\web\Response::FORMAT_JSON;
        $behavior['authenticator'] = [
            'class' => \yii\filters\auth\HttpBearerAuth::className(),
        ];
        return $behavior;
    }
        // public function actionIndex(){
        //     return [ 'status' => 'sukses',
        //     'data' => 'abc'
        //     ];
        // }

        public function actionIndex(){
            $userId = Yii::$app->user->id; 
           
            $model = User::findOne($userId);
            // var_dump($model);
            $model->generateAuthKey();
            $model->save();
            return [ 'status' => 'success',
            'data' => 'You are already logout'
            ];
        }
}
