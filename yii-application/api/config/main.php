<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'api\controllers',
    // 'homeUrl' => '/api',
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // 'baseUrl' => '/api',
            'class' => '\yii\web\Request',
           'enableCookieValidation' => false,
            // 'csrfParam' => '_csrf-api',
             'parsers' => [  
                     'application/json' => 'yii\web\JsonParser',            
         ]
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableSession' => false,
            'loginUrl' => null,
        ],
        // 'session' => [
        //     // this is the name of the session cookie used for login on the backend
        //     'name' => 'advanced-backend',
        // ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                 
            ],
        ],
        // 'authManager' => [
        //     'class'=>'yii\rbac\DbManager',
        //     'defaultRoles'=>['guest'],
        // ],
    ],
    'params' => $params,
];
