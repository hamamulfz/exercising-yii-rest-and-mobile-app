# Installation

### Yii2 REST API

To install this repository, you need to do following instruction:
1. Run `composer init`
2. Install rbac
3. Do a migration via `yii migrate` to add employee table. In the future I will add seeding for this table.

### Flutter Source Code

under development

# Overview

This repository is used to learn Yii2 Auth REST API connected with Mobile App which is developed with Flutter.
Currently this development is under progress to develop Yii REST API.


## Yii2 REST API Feature

This Yii2 REST API have some feature as below :
- Authentication
  - Login and Logout
- CRUD
  - Create, Read, Update, Delete and Search by value


### Authentication

* Login

login can be done under link `yii-application/api/web/login` with parameter i.e :
```
{
    "username" : "admin",
    "password" : "administrator"
    
}
```
currently, user only can be created via front end sign up page.


* LogOut

login can be done under link `yii-application/api/web/logout`.
logout is protected with auth, so only logged user can do this feature.

### CRUD

* Create

login can be done under link `yii-application/api/web/employee/create`.

* Read

login can be done under link `yii-application/api/web/employee/read`.

* Update

login can be done under link `yii-application/api/web/employee/update`.

* Delete

login can be done under link `yii-application/api/web/employee/delete`.

* Search

login can be done under link `yii-application/api/web/employee/search`.



## Flutter for Mobile App

Under Development


# Things Need To Do

### Yii2

- Integrate Firebase Auth
- integrate Firebase Database.
- Try to connect to Firebase Auth via OAuth2 with access token

### Flutter

- Create an App connected to Yii2 local rest api

